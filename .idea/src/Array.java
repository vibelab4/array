public class Array {
    public static void main(String[] args) {
        int[] arr = new int[8];
        for(int i=0; i < arr.length; i++){
            arr[i] = 1 + (int) (Math.random() * 10);
            System.out.print(arr[i] +" ");
        } System.out.println(" ");
        boolean stop = false;
        for (int i = 0; i < arr.length - 1 ; i++ ) {
            if (arr[i] > arr[i+1]) {
                stop = true;
                break;
            }
        } if (stop) {
            System.out.println(">> Массив не явялется строго возрастающей прогрессией.");
        }
         else {
            System.out.println(">> Массив явялется строго возрастающей прогрессией.");
        }
         for (int i = 0; i < arr.length; i++) {
             if((i % 2) == 0)
                 System.out.print(arr[i] + " ");
             else {
                 arr[i] = 0;
                 System.out.print(arr[i] + " ");
             }
        }
    }
}